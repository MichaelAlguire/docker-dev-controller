# Dev Environment Controller
This repo contains a single docker-compose.yml file that creates the foundation for a simple Docker development environment.  It uses Portainer as the Docker gui and jwilder/nginx-proxy as a reverse proxy.

Use a docker-compose.yml file in your project that works with this one to make a development environment that doesn't require setting ports.  Example coming.

## Quick Start:  
* Add portainer.dev.com to your hosts file
* Run docker-compose up -d

## Contributing
You can use this file however you like. Pull requests are welcome.

## Credit
Credit [dmitrym0](https://github.com/dmitrym0/simple-lets-encrypt-docker-compose-sample) for making the initial docker-compose.yml file I started with. 